require 'test_helper'

class DoorPermissionsControllerTest < ActionController::TestCase
  setup do
    @door_permission = door_permissions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:door_permissions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create door_permission" do
    assert_difference('DoorPermission.count') do
      post :create, door_permission: { active: @door_permission.active, door_id: @door_permission.door_id, note: @door_permission.note, user_id: @door_permission.user_id }
    end

    assert_redirected_to door_permission_path(assigns(:door_permission))
  end

  test "should show door_permission" do
    get :show, id: @door_permission
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @door_permission
    assert_response :success
  end

  test "should update door_permission" do
    put :update, id: @door_permission, door_permission: { active: @door_permission.active, door_id: @door_permission.door_id, note: @door_permission.note, user_id: @door_permission.user_id }
    assert_redirected_to door_permission_path(assigns(:door_permission))
  end

  test "should destroy door_permission" do
    assert_difference('DoorPermission.count', -1) do
      delete :destroy, id: @door_permission
    end

    assert_redirected_to door_permissions_path
  end
end
