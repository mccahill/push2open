require 'test_helper'

class MobileTokensControllerTest < ActionController::TestCase
  setup do
    @mobile_token = mobile_tokens(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mobile_tokens)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mobile_token" do
    assert_difference('MobileToken.count') do
      post :create, mobile_token: { active: @mobile_token.active, mobile_device: @mobile_token.mobile_device, netid: @mobile_token.netid, token: @mobile_token.token }
    end

    assert_redirected_to mobile_token_path(assigns(:mobile_token))
  end

  test "should show mobile_token" do
    get :show, id: @mobile_token
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mobile_token
    assert_response :success
  end

  test "should update mobile_token" do
    put :update, id: @mobile_token, mobile_token: { active: @mobile_token.active, mobile_device: @mobile_token.mobile_device, netid: @mobile_token.netid, token: @mobile_token.token }
    assert_redirected_to mobile_token_path(assigns(:mobile_token))
  end

  test "should destroy mobile_token" do
    assert_difference('MobileToken.count', -1) do
      delete :destroy, id: @mobile_token
    end

    assert_redirected_to mobile_tokens_path
  end
end
