Push2open::Application.routes.draw do
 
  resources :mobile_tokens do as_routes end
  resources :door_permissions do as_routes end
  resources :doors do as_routes end
  resources :sessions do as_routes end
  resources :admins do as_routes end
  resources :users do as_routes end

  ####### start mobile devices routes #######
  #
  # shib-protected page to issue a mobile token 
  get "home/get_mobile_token"=>'home#make_mobile_token'
  #
  # the app_install heirarchy is not shib protected, so do mobile_token calls here
  #
  # get a list of valid doors for the token user
  post '/app_install/mobile_door_list'=>'mobile_rest#doors_for_token'
  #
  # request that a door be opened
  post '/app_install/mobile_open_door'=>'mobile_rest#open_door_from_mobile'
  #
  ####### end mobile devices routes #######
  
  # accept open door commands from web page
  get 'home/open_the_door'=>'open_door#open_door_from_web'
  
  # convenient way to get a dump of the session and environment
  get 'debug' => 'home#debug'

  get "administration/index"

  get "home/index"
  match 'home/logout'=>'home#logout'

  root :to => 'home#index'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
