# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Push2open::Application.initialize!

APP_CREDS = YAML.load_file("#{Rails.root}/config/push2open-creds.yml")[Rails.env]
