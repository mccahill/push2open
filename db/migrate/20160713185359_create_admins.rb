class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.string :displayName
      t.string :duid
      t.string :netid

      t.timestamps
    end
  end
end
