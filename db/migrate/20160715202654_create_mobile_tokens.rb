class CreateMobileTokens < ActiveRecord::Migration
  def change
    create_table :mobile_tokens do |t|
      t.boolean :active
      t.string :token
      t.string :netid
      t.string :mobile_device

      t.timestamps
    end
  end
end
