class CreateDoorPermissions < ActiveRecord::Migration
  def change
    create_table :door_permissions do |t|
      t.boolean :active
      t.string :note
      t.integer :user_id
      t.integer :door_id

      t.timestamps
    end
    
    add_index :door_permissions, :user_id 
    add_index :door_permissions, :door_id
  end
end
