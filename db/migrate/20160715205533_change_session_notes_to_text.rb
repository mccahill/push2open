class ChangeSessionNotesToText < ActiveRecord::Migration
  def up
    change_column :sessions, :notes, :text
  end

  def down
    change_column :sessions, :notes, :string
  end
end
