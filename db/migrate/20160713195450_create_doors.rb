class CreateDoors < ActiveRecord::Migration
  def change
    create_table :doors do |t|
      t.string :description
      t.string :method
      t.string :parameters

      t.timestamps
    end
  end
end
