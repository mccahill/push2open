class AddLocationsToSessions < ActiveRecord::Migration
  def change
    add_column :sessions, :ip_address, :string
    add_column :sessions, :latitude, :string
    add_column :sessions, :longitude, :string
  end
end
