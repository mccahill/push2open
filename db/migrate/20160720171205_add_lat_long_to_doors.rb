class AddLatLongToDoors < ActiveRecord::Migration
  def change
    add_column :doors, :latitude_decimal, :string
    add_column :doors, :longitude_decimal, :string
  end
end
