# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160720191405) do

  create_table "admins", :force => true do |t|
    t.string   "displayName"
    t.string   "duid"
    t.string   "netid"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "door_permissions", :force => true do |t|
    t.boolean  "active"
    t.string   "note"
    t.integer  "user_id"
    t.integer  "door_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "door_permissions", ["door_id"], :name => "index_door_permissions_on_door_id"
  add_index "door_permissions", ["user_id"], :name => "index_door_permissions_on_user_id"

  create_table "doors", :force => true do |t|
    t.string   "description"
    t.string   "method"
    t.string   "parameters"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "name"
    t.string   "latitude_decimal"
    t.string   "longitude_decimal"
  end

  create_table "mobile_tokens", :force => true do |t|
    t.boolean  "active"
    t.string   "token"
    t.string   "netid"
    t.string   "mobile_device"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "action"
    t.string   "netid"
    t.text     "notes"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "ip_address"
    t.string   "latitude"
    t.string   "longitude"
  end

  create_table "users", :force => true do |t|
    t.string   "displayName"
    t.string   "duid"
    t.string   "netid"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
