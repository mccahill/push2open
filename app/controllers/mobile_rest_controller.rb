class MobileRestController < ApplicationController
  
  #
  # mobile devices get a list of the doors they can control here by
  # querying with the mobile_token (so we know which user is it)
  #
  def doors_for_token
    the_doors = DoorPermission.doors_for_token( params['mobile_token'] )
    respond_to do |format|
      format.json { render :layout => false, :json => the_doors }
    end     
  end
  
  def open_door_from_mobile
    # lookup who is making the request based on mobile_token
    token_mobile = MobileToken.find_by_token( params['mobile_token'] )
    if token_mobile.nil?
      @token_info = [:status=>:mobile_token_error, 
                     :message=>"invalid mobile token #{params['mobile_token']}"] 
    else
      token_user = User.find_by_netid( token_mobile.netid )
      if token_user.nil?
        @token_info = [:status=>:mobile_token_netid_error, 
                       :message=>"cannot find netid for mobile token #{params['mobile_token']}"] 
      else
        token_door = Door.find_by_id( params['door'] )
        if token_door.nil?
          @token_info = [:status=>:mobile_token_door_error, 
                         :message=>"cannot find door for mobile token #{params['mobile_token']}"] 
        else
          Session.create(:action => 'open_door_from_mobile_request', 
                         :netid => token_user.netid, 
                         :ip_address => request.env['REMOTE_ADDR'], 
                         :latitude => params['lat'], 
                         :longitude => params['long'],
                         :notes => "#{token_door.description}")   
          odc = OpenDoorController.new
          my_results = odc.try_to_open_the_door( token_user, token_door)
          @token_info = [:status=>"#{my_results[:status]}", :netid=>"#{token_user.netid}", 
                         :door=>"#{token_door.name}", :message=>"#{my_results[:message]}"] 
        end
      end
    end               
    respond_to do |format|
      format.json { render :layout => false, :json => @token_info }
    end            
  end
  
  
  # You can get a token from the shib protexted page at
  #   home/get_mobile_token.json?mobile_device=the_device_type_here
  #
  # after you have a token, it can be presented to fetch a list 
  # of valid doors for the user and to open a specific door
  # 
  # ------- get a list of doors -------
  #
  # curl -X POST -H "Content-Type: multipart/form-data" \
  #  -H "Accept: application/json" \
  #  --form mobile_token=8f158c80-3973-4ae2-960f-297e88b3bc90  \
  #  http://localhost:3000/app_install/mobile_door_list
  #
  # on success returns:
  #
  # [[{"name":"ATC Strickland courtyard","description":"halfway down the courtyard","id":1}],
  #  [{"name":"Simulated door","description":"Mark's desk phone","id":2}],
  #  [{"name":"OIT front desk","description":"1st floor Strickland left of the receptionist desk","id":3}]]
  # 
  # on error returns:
  #
  # [{"status":"mobile_token_error","message":"invalid mobile token 8f158c80-3973-4ae2-960f-297e88b3bc90"}]
  #
  #
  # ------- open a specific door -------
  #
  # curl -X POST -H "Content-Type: multipart/form-data" \
  #  -H "Accept: application/json" \
  #  --form mobile_token=8f158c80-3973-4ae2-960f-297e88b3bc90  \
  #  --form door=2  \
  #  http://localhost:3000/app_install/mobile_open_door 
  #
  # on success returns:
  #
  # [{"status":"success","netid":"mccahill","door":"Simulated door",
  #   "message":"door open sent to Mark's desk phone"}]
  # 
  # NOTE: errors are indicated by a status != "success"
  # and an informative message is included, for example:
  #
  # [{"status":"mobile_token_error",
  #   "message":"invalid mobile token 8f158c80-3973-4ae2-960f-297e88b3bc90"}]
  #
  # [{"status":":mobile_token_netid_error",
  #   "message":"cannot find netid for mobile token 8f158c80-3973-4ae2-960f-297e88b3bc90"}]
  # 
  # [{"status":"::mobile_token_door_error",
  #   "message":"cannot find door for mobile token 8f158c80-3973-4ae2-960f-297e88b3bc90"}]
  #
  # 
  # optionally, you can include lat and long from the phone in the request:
  #
  #     curl -X POST -H "Content-Type: multipart/form-data" \
  #          -H "Accept: application/json" \
  #          --form mobile_token=8f158c80-3973-4ae2-960f-297e88b3bc90  \
  #          --form door=2  \
  #          --form lat=35.993099999  \
  #          --form long=-78.90599999  \
  #          http://localhost:3000/app_install/mobile_open_door 
  #
  #

  
  
  private  
  
  # Override authorize in application_controller.rb
  def authorize
    
  end
  
  
end
