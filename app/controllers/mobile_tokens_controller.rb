class MobileTokensController < ApplicationController
  
  before_filter :onlyAdmin
  layout 'admin_base'

  active_scaffold :"mobile_token" do |conf|
    #conf.actions = [:list, :search, :show]
    conf.actions.exclude :delete
    
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :desc}
    conf.columns = [ :id, :netid, :active, :mobile_device,  :token, :updated_at, :created_at ]    
  end
  
  
  
end
