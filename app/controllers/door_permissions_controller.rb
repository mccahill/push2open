class DoorPermissionsController < ApplicationController
    
  before_filter :onlyAdmin
  layout 'admin_base'
  
  active_scaffold :"door_permission" do |conf|
    conf.label = 'Permissions'
    #conf.actions = [:list, :search, :show]
    conf.actions.exclude :delete
    active_scaffold_config.search.live = true   #submit search terms as we type for more feedback
    conf.list.sorting = { :id => :desc}
    conf.columns = [ :user, :active, :door, :note, :updated_at, :created_at ]
    [:user, :door].each do |c|
      conf.columns[c].form_ui = :select
    end
  end
  
end
