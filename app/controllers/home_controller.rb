class HomeController < ApplicationController

  require "securerandom"
  
  layout "home"
  
  
  def index
    @door_list = DoorPermission.doors_for_netid( @this_user.netid ) 
  end
  
  def logout
     reset_session
     return_to_me = "?returnto=" + url_for(:controller=>'home')
     redirect_to "/Shibboleth.sso/Logout?return=https://shib.oit.duke.edu/cgi-bin/logout.pl" +
       CGI.escape(return_to_me)
   end

   #
   # various phones and tablets need to register to get a long-lived token from this app
   # they do that by going to
   #      /home/get_mobile_token?mobile_device=the_device_type_here
   # which will require a shibboleth auth (so we know their netID). 
   # The mobile_device parameter on the URL allows for device-specific tokens...
   #
   # However, to make it super-simple to parse, you probably want to invoke the .json format
   # output by going here:
   #     /home/get_mobile_token.json?mobile_device=the_device_type_here
   #
   # for example:
   #     https://push2open.oit.duke.edu/home/get_mobile_token.json?mobile_device=test
   #
   # the token is returned in JSON format like this:
   #
   # [{"token":"8f158c80-3973-4ae2-960f-297e88b3bc90",
   #   "mobile_device":"test",
   #   "netid":"mccahill",
   #   "displayname":"Mark McCahill"}]
   #
   #
   def make_mobile_token
     mtoken = MobileToken.find_or_initialize_by_netid_and_mobile_device(:netid => @this_user.netid, :mobile_device => params['mobile_device'])
     mtoken.token = SecureRandom.uuid
     mtoken.mobile_device = params['mobile_device']
     mtoken.active = true
     mtoken.save
     @token_info = [:token=>mtoken.token, 
                    :mobile_device=>mtoken.mobile_device, 
                    :netid=>mtoken.netid,
                    :displayname=>@this_user.displayName]
      respond_to do |format|
        format.html { render :layout => 'minimalist' }
        format.json { render :layout => false, :json => @token_info }
      end     
   end
  
   
end
