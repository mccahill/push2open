class DoorsController < ApplicationController

  before_filter :onlyAdmin
  layout 'admin_base'
  
  active_scaffold :"door" do |conf|
    conf.actions.exclude :delete
    
  end
  
  
end
