class OpenDoorController < ApplicationController    
  
  def open_door_from_web    
    integer_doors_only = params['door_to_open'].to_i # make sure we only pass an integer
    which_door = Door.find(integer_doors_only)
    Session.create(:action => 'open_door_from_web_request', 
                   :netid => @this_user.netid, 
                   :ip_address => request.env['REMOTE_ADDR'], 
                   :notes => "#{which_door.description}") 
    my_results = self.try_to_open_the_door( @this_user, which_door)
    flash[:error] = my_results[:message]
    redirect_to "/"
  end 
  
  
  def try_to_open_the_door( some_user, some_door )
    # does this user have permission to open the door?
    ok_to_open = DoorPermission.can_this_user_open_door( some_user.netid, some_door.name ) 
    if ok_to_open[:can_open] 
      self.open_with_phone( some_user, some_door.parameters )
      return :status => :success, :message => "door open sent to #{some_door.description}"     
    else
       # permission check failed, log this and move on 
       Session.create(:action => 'denied open_door_request', :netid => some_user.netid, :notes => "#{ok_to_open[:reason]}") 
       return :status => :error, :message => "request denied: #{ok_to_open[:reason]}"   
    end
  end
  
  
  # methods for opening doors go here
  # for now, we only have one method - make a phone call
  # to run this manually use this curl command:
  #  
  # curl --header "Content-Type: text/xml;charset=UTF-8" --header "SOAPAction:makeCallSoap" \
  #      --data @soap-call.xml \
  #      https://fitz-cm-01.oit.duke.edu:8443/webdialer/services/WebdialerSoapService70
  #
  
  require 'typhoeus'
  require 'erubis'
      
  WEBDIALER_SERVICE_URL = "https://fitz-cm-01.oit.duke.edu:8443/webdialer/services/WebdialerSoapService70"
  
  SOAP_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
                    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:WD70">
                     <soapenv:Header/>
                     <soapenv:Body>
                      <urn:makeCallSoap soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                       <in0 xsi:type="urn:Credential">
                        <userID xsi:type="xsd:string"><%= door_app %></userID>
                        <password xsi:type="xsd:string"><%= door_password %></password>
                       </in0>
                       <in1 xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"><%= door_phone %></in1>
                       <in2 xsi:type="urn:UserProfile">
                        <user xsi:type="xsd:string"><%= door_app %></user>
                        <deviceName xsi:type="xsd:string"><%= door_device %></deviceName>
                        <lineNumber xsi:type="xsd:string"><%= door_phone %></lineNumber>
                        <supportEM xsi:type="xsd:boolean">false</supportEM>
                        <locale xsi:type="xsd:string">?</locale>
                        <dontAutoClose xsi:type="xsd:boolean">true</dontAutoClose>
                        <dontShowCallConf xsi:type="xsd:boolean">false</dontShowCallConf>
                       </in2>
                      </urn:makeCallSoap>
                     </soapenv:Body>
                    </soapenv:Envelope>'

  def open_with_phone( the_user, the_phone )
    # substitute the variables into the generic Cisco web dialer template to make a SOAP payload 
    payload = Erubis::Eruby.new(SOAP_TEMPLATE).result(:door_app => APP_CREDS['door_app'], 
                                 :door_password => APP_CREDS['door_password'],
                                 :door_device => APP_CREDS['door_device'],
                                 :door_phone  => the_phone)
                            
    # SOAP post to invoke the web dialer
    # 
    # if this turns into a high volume serivce, we can use the parallelism of  Typhoeus,
    # but for now, a single call is fine. For details on how to do the fancy
    # parallel stuff, see
    #    https://www.reinteractive.net/posts/3-rolling-your-own-ruby-soap-client-with-typhoeus-and-nokogiri     
    #                      
    response = Typhoeus::Request.post(WEBDIALER_SERVICE_URL,
                            :body    =>  payload,
                            :headers => {'Content-Type' => 'text/xml;charset=UTF-8', 
                                         'SOAPAction' => 'makeCallSoap' })
    
    # parse the response looking for success
    if response.body =~ /<responseDescription xsi:type="xsd:string">Success</ then
      Session.create(:action => 'web-dialer-success', :netid => the_user.netid, :notes => "#{the_phone}")      
    else
      Session.create(:action => 'web-dialer-fail', :netid => the_user.netid, :notes => "#{response.body}")      
    end
  end
  
   
  
end
