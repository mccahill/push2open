class Session < ActiveRecord::Base
  attr_accessible :action, :netid, :notes, :ip_address, :latitude, :longitude
end
