class User < ActiveRecord::Base
  attr_accessible :displayName, :duid, :netid
  
  has_many :door_permissions 
#  has_many :doors, through: :door_permissions 

  # ActiveScaffold will automatically use this name method to describe a 
  # record of this type
  def name
    "#{netid} (#{displayName})"
  end
   
end
