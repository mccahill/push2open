class DoorPermission < ActiveRecord::Base
  attr_accessible :active, :door_id, :note, :user_id
  belongs_to :user 
  belongs_to :door 
     
  # ActiveScaffold will automatically use this name method to describe a 
  # record of this type
  def name
    "uniqueid #{id}"
  end
       
  # we can deactivate permissions, but don't delete them
  #
  #  def authorized_for_delete?
  #     false
  #  end


  # class-level method to determine if a door open is authorized
  def self.can_this_user_open_door( a_netid, a_door_name ) 
    the_user = User.find_by_netid( a_netid )
    if the_user.nil?
      return :can_open => false, 
             :reason => "netid #{a_netid} not found"
    else
      the_door = Door.find_by_name( a_door_name )
      if the_door.nil?
        return :can_open => false, 
               :reason => "door #{a_door_name} not found"
      else
        permission_exists = DoorPermission.find_by_sql ["SELECT * FROM door_permissions WHERE active = true and user_id = ? and door_id = ?", the_user.id, the_door.id]
        if permission_exists.first.nil?
          return :can_open => false, 
                 :reason => "cannot find active permission for netid #{a_netid} and door #{a_door_name}"
        else
          return :can_open => true,  
                 :reason => "valid permission id #{permission_exists.first.id} for netid #{a_netid} and door #{a_door_name}"
        end
      end
    end
  end
 
  # list of authorized doors for the mobile device token
  def self.doors_for_token( a_token ) 
    door_list = []
    this_token = MobileToken.find_by_token( a_token )
    if this_token.nil?
      door_list = [:error=>"invalid mobile token #{a_token}"]
    else
      if this_token.active == false
        door_list = [:error=>"deactivated mobile token #{a_token}"]        
      else 
        this_user = User.find_by_netid( this_token.netid )
        some_doors = DoorPermission.find_all_by_user_id( this_user.id )         
        some_doors.each do |item|
          if item.active 
            door_list.push ([:name=>Door.find(item.door_id).name, 
                             :description=>Door.find(item.door_id).description, 
                             :lat=>Door.find(item.door_id).latitude_decimal,
                             :long=>Door.find(item.door_id).longitude_decimal,
                             :id=>item.door_id])
          end
        end
        if door_list.length == 0 
          door_list = [:error=>"no authorized doors found for #{this_user.displayName} - (#{this_user.netid})"]  
        end
      end
    end
    return door_list
  end
 
 
  # list of authorized doors for the netid
  def self.doors_for_netid( a_netid ) 
    the_user = User.find_by_netid( a_netid )
    unless the_user.nil?
      the_doors = Door.find_by_sql ["SELECT doors.id, doors.name, doors.description, doors.method, doors.parameters 
                                     FROM doors, door_permissions WHERE doors.id = door_permissions.door_id 
                                     AND active = true AND user_id = ? ", the_user.id]
      return the_doors
    end
    return []
  end
 
       
end
