class Door < ActiveRecord::Base
  attr_accessible :name, :description, :method, :parameters, :latitude_decimal, :longitude_decimal
  
  has_many :door_permissions
#  has_many :users, through: :door_permissions 
  
end
