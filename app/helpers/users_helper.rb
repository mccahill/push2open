module UsersHelper
  
  def user_door_permissions_column(record, column)
     some_permissions = DoorPermission.find_all_by_user_id(record.id)
     if some_permissions.length == 0
       return 
     else
       return("#{some_permissions.length} doors")
     end
  end
    
end
