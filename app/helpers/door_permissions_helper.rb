module DoorPermissionsHelper
  
  def door_permission_door_column(record, column)
     "#{record.door.name}: #{record.door.description}"
  end
  
end